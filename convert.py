import xlsxwriter
import xmltodict
from datetime import datetime

year = datetime.now().year - 1
workbook = xlsxwriter.Workbook(f"{year}_stocks_earnings_calculation.xlsx")

worksheet_a = workbook.add_worksheet()
worksheet_a.write_row(0, 0, ["ISIN", "Transaction Date", "Exchange Rate", "Count", "Market Rate EUR", "Transaction Fees EUR", "Total EUR", "Total JPN", "Aqusition Cost EUR", "Earnings EUR", "Earnings JPN" ])

ws = workbook.add_worksheet("Exchange Rate")

jpy = None
with open("jpy_exchange_rate_historical.xml") as jpyxml:
    jpy = xmltodict.parse(jpyxml.read())


rate_list = jpy["CompactData"]["DataSet"]["Series"]["Obs"]

for i, data in enumerate(rate_list):
    ws.write(i, 0, data["@TIME_PERIOD"])
    ws.write(i, 1, float(data["@OBS_VALUE"]))

workbook.close()
